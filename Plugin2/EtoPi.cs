﻿using System;
using System.ComponentModel;

namespace Plugin2
{
	[Description("Returns E^Pi")]
	[DisplayName("E^Pi")]
	public class EtoPi : PluginBase.IPlugin
	{
		public double Calculate(string expression)
		{
			return Math.Pow(Math.E, Math.PI);
		}
	}
}
