﻿using System;
using System.ComponentModel;

namespace Plugin2
{
	[Description("Returns Pi^2 or E^2 based on input")]
	[DisplayName("Pi^2 or E^2")]
	public class Plugin2 : PluginBase.IPlugin
	{
		public double Calculate(string expression)
		{
			switch (expression)
			{
				case "pi":
					return Math.Pow(Math.PI, 2);
				case "e":
					return Math.Pow(Math.E, 2);
				default:
					break;
			}

			return double.NaN;
		}
	}
}
