﻿using System;
using System.ComponentModel;

namespace Plugin1
{
	[Description("Returns Pi or E based on input")]
	[DisplayName("Pi or E")]
	public class Plugin1 : PluginBase.IPlugin
	{
		public double Calculate(string expression)
		{
			switch (expression)
			{
				case "pi":
					return Math.PI;
				case "e":
					return Math.E;
				default:
					break;
			}

			return double.NaN;
		}
	}
}
