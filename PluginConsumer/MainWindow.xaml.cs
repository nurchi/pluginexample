﻿using PluginBase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PluginConsumer
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
		}

		private void Btn_Refresh_Click(object sender, RoutedEventArgs e)
		{
			if (!Directory.Exists("./Plugins"))
				Directory.CreateDirectory("./Plugins");

			var files = Directory.GetFiles("./Plugins", "*.dll");

			var items = new List<KeyValuePair<string, IPlugin>>(10); // initial capacity, will grow as needed

			foreach (var file in files)
			{
				var asm = Assembly.LoadFrom(file);

				// extract the Description and DisplayName attributes if available
				var tmp_items = asm
					.GetTypes()
					.Where(t => t.GetTypeInfo().ImplementedInterfaces.Contains(typeof(IPlugin)))
					.Select(t => new { Type = t, Desc = t.GetCustomAttribute(typeof(DescriptionAttribute)), DispName = t.GetCustomAttribute(typeof(DisplayNameAttribute)) })
					.Select(t => new { t.Type, Desc = (t.Desc is DescriptionAttribute da ? da.Description : t.Type.Name), DisplayName = (t.DispName is DisplayNameAttribute dna ? dna.DisplayName : t.Type.Name) });

				items.AddRange(tmp_items.Select(t => new KeyValuePair<string, IPlugin>(t.DisplayName, (IPlugin)Activator.CreateInstance(t.Type))));
			}
			
			this.Cmb_Plugins.ItemsSource = null;
			this.Cmb_Plugins.DisplayMemberPath = "Key";
			this.Cmb_Plugins.SelectedValuePath = "Value";
			this.Cmb_Plugins.ItemsSource = items;
		}

		private void Btn_Calculate_Click(object sender, RoutedEventArgs e)
		{
			if (!(this.Cmb_Plugins.SelectedValue is IPlugin plugin)) return;

			this.Lbl_Result.Content = plugin.Calculate(this.TB_Input.Text).ToString();
		}
	}
}
