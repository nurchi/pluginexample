﻿using System;

namespace PluginBase
{
	public interface IPlugin
	{
		double Calculate(string expression = null);
	}
}
